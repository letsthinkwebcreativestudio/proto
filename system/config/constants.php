<?php

/**
 * Collection and definition of the app's constants
 */

define( 'SYSPATH', dirname( __DIR__ ) );

$APPPATH = explode('system', SYSPATH );

define( 'APPPATH', $APPPATH[0] . 'app' );

define( 'CPATH', APPPATH . '/controllers/' );

define( 'REQUEST', $_SERVER['REQUEST_URI'] );

define( 'ENV', 'development' );