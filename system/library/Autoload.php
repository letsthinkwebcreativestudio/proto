<?php 
namespace System\Library;
	
use System\System;
	
class Autoload extends System{

	public $param;
	private $config;

	public function __construct($param){
		$this->param = $param;
	}


	public function get_config(){
		return $this->config;
	}


	protected function find_config(){

	}

	protected function set_config_parameters(){

	}

	private function set_config($config){
		$this->config = $config;
	}

}