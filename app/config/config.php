<?php 

/**
 * Autoload config files
 * Files must be seperated by a comma
 */

// Custom config files
// example:
// name: 		database
// filename: 	database.config.php
// 
// path: /app/config/
$config['autoload']['config'] = '';

// Classes that are inside services folder
// example:
// name: 		Database_Service
// filename:	Database_Service.php
// 
// path: /app/services/
$config['autoload']['services'] = 'database';

// Classes that are inside the helpers folder
// example: 
// name:		String_Helper
// filename:	String_Helper.php
// 
// path: /app/helpers
$config['autoload']['helpers'] = '';

/**
 * Application Custom Routes
 * Overrides default behaviour of Controllers
 */
// example:
// name: 		home
// 
// path: /app/routes
$config['routes']['home'] = 'home'; 



