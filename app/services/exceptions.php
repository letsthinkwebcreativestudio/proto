<?php 

$exception_messages = 
	array(
		'config' => 
			array(
				'message' => 'Missing configuration file',
				'code'	=> 1
				),

		'controller' => 
			array(
				'message' => 'Invalid controller call',
				'code'	=> 1
				),

		'method' =>
			array(
				'message' => 'Method is not supported',
				'code' => 0
				)		 

	);