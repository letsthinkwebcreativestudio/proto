<?php
$loader = require 'vendor/autoload.php';
include 'system/config/constants.php';
include 'system/config/config.php';


$system = new System\System();
	
	try{
		
		// Check if environment is defined
		if(ENV){

			switch (ENV) {
				
				case 'development':
					# code...
					
					error_reporting(E_ALL & ~E_NOTICE);

					break;
				

				case 'staging':

					error_reporting(E_ERROR);

					break;

				case 'production':

					error_reporting(0);
				
					break;	

				default:
					# code...
					break;
			
			}
		}else{
		
			throw new Exception("System error: environment wasn't defined", 301);
			exit;

		}


		// We'll have to create a router method in here
		$request = REQUEST;


		// Handle the routes
		$route = explode( 'index.php', $request );

		// We'll catch the arrays and handle the 2nd index
		$routes = $route[1];

		// Define controllers
		// we will limit the results to only two
		// to identify the main class and method
		// @todo define a method here for routes that is within a config file version 2
		if( isset($routes) || $routes !== null ){

			$controller = explode( '/', $routes );
			$controller_class = ($controller[1]) ? ucfirst($controller[1]) : null;
			$controller_method = ($controller[2]) ? $controller[2] : null;

			// Check if a controller class exists
			if( $controller_class !== null ){
				
				// Assign the Controllers namespace
				// then autoload the controller
				
				//@todo insert the namespaces in one config file
				$class = "Application\\Controllers\\".$controller_class;
				$new_controller = new $class();
				if( $controller_method !== null ){

					return $new_controller->$controller_method();

				}else{

					return $new_controller->index();

				}
			}

		}
		


		


	}catch (Exception $e){

		exit($e->getMessage());
	
	}


/*try{


	$request 		= $_REQUEST;
	$request_method	= $_SERVER['REQUEST_METHOD']; 

	$controller 	= strtolower($request['con']);
	$action 		= strtolower($request['a']).'_'.(strtolower($request_method));


	if(file_exists("config.php")){
		include_once 'config.php';
	}else{
		throw new Exception($exception_messages['config']['message'], $exception_messages['config']['code']);
	}

	if(file_exists("controllers/{$controller}.php")){
		include_once "controllers/{$controller}.php";
	}else{
		throw new Exception($exception_messages['controller']['message'], $exception_messages['controller']['code']);
	}

	$controller 	= ucfirst($controller);
	$con_instance 	= new $controller($request);

	if( method_exists($con_instance, $action) === false){
		throw new Exception($exception_messages['method']['message'], $exception_messages['method']['code']);
	}

	$response = 
		array(
			'data' 		=> $con_instance->$action(),
			'success' 	=> true
		);

} catch( Exception $e ){

	$response = 
		array(
			'success' 	=> false,
			'error' 	=>  $e->getMessage()
			);
	
}
echo json_encode($response);
exit();*/